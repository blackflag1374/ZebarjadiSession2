package com.example.admin.loginregisterapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    Button bLogin;
    EditText editUsername, editPassword;
    TextView tvRegistering;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        editUsername = (EditText) findViewById(R.id.editUsername);
        editPassword = (EditText) findViewById(R.id.editPassword);
        bLogin = (Button) findViewById(R.id.bLogin);
        tvRegistering =(TextView) findViewById(R.id.tvRegistering) ;
                bLogin.setOnClickListener(this);
        tvRegistering.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bLogin:

                break;
            case R.id.bLogout:

                break;
            case R.id.tvRegistering:
                 startActivity(new Intent(this,RegisterActivity.class));


                break;


        }
    }
}